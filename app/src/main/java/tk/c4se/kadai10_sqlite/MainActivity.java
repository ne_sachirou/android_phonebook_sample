package tk.c4se.kadai10_sqlite;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Database.init(this);
        final MainActivity context = this;
        ((ListView) findViewById(R.id.listViewBook)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PhonebookItem item = PhonebookItem.findNth(position);
                if (null == item) {
                    return;
                }
                Intent intent = new Intent(context, EditItemActivity.class);
                intent.putExtra("ID", item.getId());
                intent.putExtra("NAME", item.getName());
                intent.putExtra("PHONE", item.getPhone());
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateListView();
    }

    public void register(View v) {
        Editable editName = ((EditText) findViewById(R.id.editTextName)).getText();
        Editable editPhone = ((EditText) findViewById(R.id.editTextPhone)).getText();
        PhonebookItem item = new PhonebookItem();
        item.setName(editName.toString().trim());
        item.setPhone(editPhone.toString().trim());
        try {
            item.validate();
        } catch (ValidationException ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        editName.clear();
        editPhone.clear();
        try {
            item.save();
        } catch (SQLiteConstraintException ex) {
            Toast.makeText(this, "データを追加できません", Toast.LENGTH_SHORT).show();
            editName.append(item.getName());
            editPhone.append(item.getPhone());
        }
        updateListView();
    }

    public void deleteAll(View v) {
        PhonebookItem.destroyAll();
        updateListView();
    }

    public void updateListView() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        for(PhonebookItem item: PhonebookItem.all()) {
            adapter.add(String.format("%6s\t%s", item.getName(), item.getPhone()));
        }
        ((ListView) findViewById(R.id.listViewBook)).setAdapter(adapter);
    }
}
