package tk.c4se.kadai10_sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by nesachirou on 15/02/25.
 */
public class Database {
    private static Database ourInstance;

    public static synchronized void init(Context context) {
        if (null == ourInstance) {
            ourInstance = new Database(context);
        }
    }

    public static Database getInstance() {
        return ourInstance;
    }

    private SQLiteDatabase db;

    private Database(Context context) {
        db = context.openOrCreateDatabase("kagai10_sqlite.db", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS phonebook(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, phone TEXT NOT NULL UNIQUE)");
    }

    public SQLiteDatabase openDb() {
        return db;
    }
}
