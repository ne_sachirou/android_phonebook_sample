package tk.c4se.kadai10_sqlite;

import lombok.Getter;

/**
 * Created by nesachirou on 15/02/25.
 */
public class ValidationException extends Exception {
    @Getter
    private String message;

    public ValidationException(String message){
        super();
        this.message = message;
    }
}
