package tk.c4se.kadai10_sqlite;

import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * Created by nesachirou on 15/02/25.
 */
@Data
public class PhonebookItem {
    public static List<PhonebookItem> all() {
        List<PhonebookItem> items = new ArrayList<>();
        Cursor c = Database.getInstance().openDb().rawQuery("SELECT id, name, phone FROM phonebook", null);
        while (c.moveToNext()) {
            PhonebookItem item = new PhonebookItem();
            item.setId(c.getInt(c.getColumnIndex("id")));
            item.setName(c.getString(c.getColumnIndex("name")));
            item.setPhone(c.getString(c.getColumnIndex("phone")));
            items.add(item);
        }
        return items;
    }

    public static PhonebookItem findNth(int position) {
        Cursor c = Database.getInstance().openDb().rawQuery("SELECT id, name, phone FROM phonebook LIMIT 1 OFFSET ?", new String[]{String.format("%d", position)});
        if (!c.moveToNext()) {
            return null;
        }
        PhonebookItem item = new PhonebookItem();
        item.setId(c.getInt(c.getColumnIndex("id")));
        item.setName(c.getString(c.getColumnIndex("name")));
        item.setPhone(c.getString(c.getColumnIndex("phone")));
        return item;
    }

    public static void destroyAll() {
        Database.getInstance().openDb().execSQL("DELETE FROM phonebook");
    }

    private int id;
    private String name;
    private String phone;

    public void validate() throws ValidationException {
        if (name.isEmpty()) {
            throw new ValidationException("名前は必須です");
        }
        if (phone.isEmpty()) {
            throw new ValidationException("電話番号は必須です");
        }
    }

    public void save() throws SQLiteConstraintException {
        if (0 == id) {
            Database.getInstance().openDb().execSQL("INSERT INTO phonebook(name, phone) values (?, ?)", new Object[]{name, phone});
        } else {
            Database.getInstance().openDb().execSQL("UPDATE phonebook SET name = ?, phone = ? WHERE id = ?", new Object[]{name, phone, id});
        }
    }

    public void destroy() {
        if (0 == id) {
            return;
        }
        Database.getInstance().openDb().execSQL("DELETE FROM phonebook WHERE id = ?", new Object[]{id});
        id = 0;
    }
}
