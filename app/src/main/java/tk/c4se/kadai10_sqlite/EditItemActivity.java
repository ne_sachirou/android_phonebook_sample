package tk.c4se.kadai10_sqlite;

import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class EditItemActivity extends ActionBarActivity {
    private PhonebookItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item);
        Database.init(this);
        Intent intent = getIntent();
        item = new PhonebookItem();
        item.setId(intent.getIntExtra("ID", 0));
        item.setName(intent.getStringExtra("NAME"));
        item.setPhone(intent.getStringExtra("PHONE"));
        Editable editName = ((EditText) findViewById(R.id.editTextName)).getText();
        Editable editPhone = ((EditText) findViewById(R.id.editTextPhone)).getText();
        editName.append(item.getName());
        editPhone.append(item.getPhone());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void saveItem(View v) {
        Editable editName = ((EditText) findViewById(R.id.editTextName)).getText();
        Editable editPhone = ((EditText) findViewById(R.id.editTextPhone)).getText();
        item.setName(editName.toString().trim());
        item.setPhone(editPhone.toString().trim());
        try {
            item.validate();
        } catch (ValidationException ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            item.save();
        } catch (SQLiteConstraintException ex) {
            Toast.makeText(this, "データを保存できません", Toast.LENGTH_SHORT).show();
            return;
        }
        finish();
    }

    public void deleteItem(View v) {
        item.destroy();
        finish();
    }

    public void cancelEdit(View v) {
        finish();
    }
}
